#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num, i, j;
    printf("Enter an integer: ");
    scanf("%d", &num);
    printf("The multiplication table from 1 to %d are : \n",num);
    for (i = 1; i <= num; i = i + 1)
    {
        for (j = 1; j <= 12; j = j + 1)
        {
            printf("%d * %d = %d\n", i,j,i * j);
        }
        printf("\n");
    }
    return 0;
}
