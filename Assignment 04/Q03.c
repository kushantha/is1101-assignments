#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num, sum = 0, mod;
    printf("Enter a number: ");
    scanf("%d", &num);
    while(num != 0)
    {
        mod = num%10;
        sum = sum * 10 + mod;
        num = num / 10;
    }
    printf("Reversed Number: %d",sum);
    return 0;
}
