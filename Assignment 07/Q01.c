#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 100

int main()
{
    int len, i, pos;
    char rev[MAX_SIZE];
    char str[MAX_SIZE];
    printf("Enter the sentence :\n");
    fgets(str, MAX_SIZE, stdin);
    len = strlen(str);
    pos = len;

    for(i = 0; i <= len; i++)
    {
        pos = pos - 1;
        rev[i] = str[pos];
    }
    rev[len + 1] = '\0';
    printf("\nThe reversed sentence is");
    puts(rev);
    return 0;
}
