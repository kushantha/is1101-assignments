#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CHAR_SIZE 100

struct  profile
{
    char name[MAX_CHAR_SIZE];
    char subject[MAX_CHAR_SIZE];
    int marks;
};

int main()
{
    struct profile pr[10];
    int i;

    for(i = 0; i < 5; i = i + 1)
    {
        printf("\nEnter the name of the student : ");
        scanf("%s", pr[i].name);
        printf("\nEnter the subject : ");
        scanf("%s", pr[i].subject);
        printf("\nEnter the marks of the subject : ");
        scanf("%d", &pr[i].marks);
    }

    for(i = 0; i < 5; i = i + 1)
    {
        printf("\n%s scored %d marks in %s subject.\n", pr[i].name, pr[i].marks, pr[i].subject);
    }
    return 0;
}
